package com.rinira.bldemo;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.rinira.bldemo.adapter.BluetoothDeviceListAdapter;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class BluetoothActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    public static final String TAG = BluetoothActivity.class.getSimpleName();

    BluetoothConnectionService mBluetoothConnection;

    EditText mEdtMsg;
    Button btnSend;
    ListView lvPairedDevices;

    LinearLayout layoutList;
    BluetoothAdapter mBluetoothAdapter;
    ArrayList<BluetoothDevice> arrayBluetoothDevices;
    BluetoothDeviceListAdapter mDeviceListAdapter;

    boolean isPaired = false;

    private final BroadcastReceiver mBroadcastReceiverBonded = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if(action.equals(BluetoothDevice.ACTION_BOND_STATE_CHANGED)) {
                BluetoothDevice mDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                //3 cases:
                //case1: bonded already
                if (mDevice.getBondState() == BluetoothDevice.BOND_BONDED){
                    Log.d(TAG, "BroadcastReceiver: BOND_BONDED.");
                    isPaired = true;
                    Toast.makeText(BluetoothActivity.this, "BONDED", Toast.LENGTH_LONG).show();

                    //inside BroadcastReceiver4
                    //inside BroadcastReceiver4

                    mBluetoothConnection = new BluetoothConnectionService(BluetoothActivity.this);

                  //  btnSend.setVisibility(View.VISIBLE);
                }
                //case2: creating a bone
                if (mDevice.getBondState() == BluetoothDevice.BOND_BONDING) {
                    Toast.makeText(BluetoothActivity.this, "BONDING", Toast.LENGTH_LONG).show();
                    Log.d(TAG, "BroadcastReceiver: BOND_BONDING.");
                }
                //case3: breaking a bond
                if (mDevice.getBondState() == BluetoothDevice.BOND_NONE) {

                    Toast.makeText(BluetoothActivity.this, "FAILED TO BOND", Toast.LENGTH_LONG).show();
                    Log.d(TAG, "BroadcastReceiver: BOND_NONE.");
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ble);

        // Bluetooth

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        arrayBluetoothDevices = new ArrayList<BluetoothDevice>();

        btnSend = (Button) findViewById(R.id.btnSend);
        btnSend.setOnClickListener(this);
       // btnSend.setVisibility(View.GONE);

        layoutList = (LinearLayout) findViewById(R.id.layoutList);
        lvPairedDevices = (ListView) findViewById(R.id.lvPairedDevices);
        lvPairedDevices.setOnItemClickListener(this);

        isPaired = false;
    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        registerReceiver(mBroadcastReceiverBonded, filter);

        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

        for(BluetoothDevice device : pairedDevices) {

            if (arrayBluetoothDevices.contains(device)) {
            } else {
                arrayBluetoothDevices.add(device);
            }
        }
        if (arrayBluetoothDevices.size() > 0) {

            mDeviceListAdapter = new BluetoothDeviceListAdapter(this, R.layout.device_ble_adapter_view, arrayBluetoothDevices);
            lvPairedDevices.setAdapter(mDeviceListAdapter);

        } else {

            layoutList.setVisibility(View.GONE);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mBroadcastReceiverBonded != null) {
            unregisterReceiver(mBroadcastReceiverBonded);
        }
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.btnSend) {

            if (isPaired) {

                if (!mEdtMsg.getText().toString().trim().equalsIgnoreCase("")) {
                    byte[] bytes = mEdtMsg.getText().toString().getBytes(Charset.defaultCharset());
                    mBluetoothConnection.write(bytes);
                    mEdtMsg.setText("");
                } else {
                    Toast.makeText(this, "Please enter text to send.", Toast.LENGTH_LONG).show();
                }
            } else {

                Toast.makeText(this, "Connection Lost.", Toast.LENGTH_LONG).show();

            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long timestamp) {

        isPaired = true;
        mBluetoothConnection = new BluetoothConnectionService(BluetoothActivity.this);
        btnSend.setVisibility(View.VISIBLE);

    }
}
